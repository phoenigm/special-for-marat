package ru.kpfu.itis.webchat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.webchat.domain.Message;

public interface MessageRepository extends JpaRepository<Message, Long> {
}
