package ru.kpfu.itis.webchat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.webchat.domain.Member;
import ru.kpfu.itis.webchat.repository.MemberRepository;

import java.util.UUID;

@Service
public class MemberService {
    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public void registration(String username, String password) {
        memberRepository.save(Member.builder()
                .username(username)
                .password(passwordEncoder.encode(password))
                .build()
        );
    }

    public String login(String username, String password) {
        Member member = memberRepository.findByUsername(username)
                .orElseThrow(() -> new BadCredentialsException("Bad credentials"));

        if (!passwordEncoder.matches(password, member.getPassword())) {
            throw new BadCredentialsException("Bad credentials");
        }
        String token = UUID.randomUUID().toString();
        member.setToken(token);
        memberRepository.save(member);
        return token;
    }

    public Member findMemberByToken(String token) {
        return memberRepository.findByToken(token)
                .orElseThrow(() -> new UsernameNotFoundException("Wrong token"));
    }
}
