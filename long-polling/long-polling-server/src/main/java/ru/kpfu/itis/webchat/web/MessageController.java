package ru.kpfu.itis.webchat.web;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.kpfu.itis.webchat.domain.Message;
import ru.kpfu.itis.webchat.service.MemberService;
import ru.kpfu.itis.webchat.service.MessageService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class MessageController {
    private final Map<String, List<Message>> subscribers = new HashMap<>();

    private final MessageService messageService;
    private final MemberService memberService;

    public MessageController(MessageService messageService, MemberService memberService) {
        this.messageService = messageService;
        this.memberService = memberService;
    }

    @GetMapping("/messages")
    public List<Message> messageList() {
        return messageService.getMessages();
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/messages/new")
    public List<Message> newMessages() throws InterruptedException {
        String token = SecurityContextHolder.getContext()
                .getAuthentication()
                .getName();
        subscribers.putIfAbsent(token, new ArrayList<>());
        synchronized (subscribers.get(token)) {
            if (subscribers.get(token).isEmpty()) {
                subscribers.get(token).wait();
            }
            List<Message> newMessages = new ArrayList<>(subscribers.get(token));
            subscribers.get(token).clear();
            return newMessages;
        }
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/messages")
    public ResponseEntity<?> saveMessage(@RequestBody String message) {
        String token = SecurityContextHolder.getContext()
                .getAuthentication()
                .getName();
        subscribers.putIfAbsent(token, new ArrayList<>());
        Message addedMessage = messageService.saveMessage(
                Message.builder()
                        .text(message)
                        .member(memberService.findMemberByToken(token))
                        .build()
        );
        for (List<Message> pageMessages : subscribers.values()) {
            synchronized (pageMessages) {
                pageMessages.add(addedMessage);
                pageMessages.notifyAll();
            }
        }
        return ResponseEntity.ok().build();
    }
}
