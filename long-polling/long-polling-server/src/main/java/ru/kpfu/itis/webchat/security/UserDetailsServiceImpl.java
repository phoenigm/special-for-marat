package ru.kpfu.itis.webchat.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.kpfu.itis.webchat.domain.Member;
import ru.kpfu.itis.webchat.repository.MemberRepository;

public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private MemberRepository memberRepository;

    @Override
    public UserDetails loadUserByUsername(String token) throws UsernameNotFoundException {
        Member member = memberRepository.findByToken(token)
                .orElseThrow(() -> new UsernameNotFoundException("Fuck u"));
        return new UserDetailsImpl(member);
    }

}
