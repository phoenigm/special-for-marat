package ru.kpfu.itis.webchat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
@SpringBootApplication
public class LongPollingServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(LongPollingServerApplication.class, args);
    }

}
