plugins {
    java
    id("org.springframework.boot")
}

group = "ru.kpfu.itis"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web:2.2.0.RELEASE")
    implementation("org.springframework.boot:spring-boot-starter-security:2.2.0.RELEASE")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa:2.2.0.RELEASE")
    implementation("org.postgresql:postgresql:42.2.8")
    
    compileOnly("org.projectlombok:lombok:1.18.10")
    annotationProcessor("org.projectlombok:lombok:1.18.10")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
}