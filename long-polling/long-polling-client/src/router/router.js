import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/components/home/Home'
import SignIn from '@/components/signIn/SignIn'
import SignUp from '@/components/signUp/SignUp'
import Profile from '@/components/profile/Profile'
import Dialogs from '@/components/messages/Dialogs'
import Search from '@/components/search/Search'

Vue.use(Router);

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Home,
            meta: {nonRequiresAuth: true}
        },
        {
            path: '/signIn',
            component: SignIn,
            meta: {nonRequiresAuth: true}
        },
        {
            path: '/signUp',
            component: SignUp,
            meta: {nonRequiresAuth: true}
        },
        {
            path: '/profile',
            component: Profile,
        },
        {
            path: '/messages',
            component: Dialogs,
        },
        {
            path: '/search',
            component: Search,
        }
    ]
});

router.beforeEach((to, from, next) => {
    const requiresAuth = !to.matched.some(record => record.meta.nonRequiresAuth);
    const token = localStorage.getItem('access_token') || '';

    console.log('Require auth: ' + requiresAuth);
    console.log('Access token: ' + token);
    if (requiresAuth && !token) {
        next('/signIn')
    } else if (!requiresAuth && token) {
        next('/profile')
    } else {
        next()
    }
});

export default router;
