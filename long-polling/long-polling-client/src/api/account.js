import axios from '../api/axios.config'

const api = axios.create({
    baseURL: 'http://localhost:9000',
    headers: {
        'Content-Type': 'application/json',
    }
});

api.interceptors.request.use(
    config => {
        const accessToken = localStorage.getItem('access_token');

        if (accessToken) {
            config.headers.authorization = accessToken;
        }
        return config;
    }, error => error
);

/*api.interceptors.response.use(
    response => response,
    error => {
        if (error.response.status === 401) {
            store.dispatch('refreshToken')
                .then(() => {
                    router.go(0);
                });
        }
        return Promise.reject(error);
    }
);*/

export default {
    getMy: () => api.get('/my'),
    create: accountDetails => api.post('/register', JSON.stringify(accountDetails)),
    login: accountDetails => api.post('/login', JSON.stringify(accountDetails)),
    getMessages: () => api.get("/messages"),
    sendMessage: (message) => api.post("/messages", message)
}