import api from "../api/account";

const account = {
    state: {
        account: {},
        token: '',
        messages: []
    },

    mutations: {
        updateAccount(state, account) {
            state.account = account
        },
        updateToken(state, token) {
            state.token = token;
            localStorage.setItem("access_token", token)
        },
        updateMessages(state, messages) {
            state.messages = messages;
        },
        addMessages(state, messages) {
            state.messages = state.messages.concat(messages)
        },
        addMessage(state, message) {
            state.messages.push(message);
        }
    },

    actions: {
        async fetchMyAccount({commit}) {
            const response = await api.getMy();
            commit('updateAccount', response.data);
        },

        async fetchMessages({commit}) {
            const response = await api.getMessages();
            console.log(response.data);
            commit('updateMessages', response.data)
        },

        async sendMessage({commit}, message) {
            const response = await api.sendMessage(message);
            commit('addMessage', response.data)
        },

        async login({commit}, formDetails) {
            const response = await api.login(formDetails);
            commit('updateToken', response.data);
        },

        async signUp({commit}, formDetails) {
            try {
                console.log(JSON.stringify(formDetails));
                await api.create(formDetails);
            } catch (error) {
                throw error;
            }
        },
        async logout({commit}) {
            await localStorage.removeItem("access_token")
        }
    }
};

export default account;
