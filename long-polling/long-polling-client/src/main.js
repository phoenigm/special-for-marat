import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@fortawesome/fontawesome-free/css/all.css'
import vueHeadful from 'vue-headful'

import store from '@/store/store'
import router from '@/router/router'

Vue.config.productionTip = false;

Vue.use(Vuetify);
Vue.component('vue-headful', vueHeadful);

new Vue({
    store,
    router,
    render: h => h(App),
}).$mount('#app');
