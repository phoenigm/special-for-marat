package ru.kpfu.itis.webchat.security;

import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Optional;

public class JwtAuthenticationFilter implements Filter {

    @Override
    public void doFilter(
            ServletRequest request,
            ServletResponse response,
            FilterChain chain
    ) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        Optional<String> tokenOptional = Optional.ofNullable(httpRequest.getHeader("Authorization"));

        tokenOptional.ifPresent(token -> SecurityContextHolder.getContext()
                .setAuthentication(new JwtAuthentication(token)));

        chain.doFilter(request, response);
    }
}
