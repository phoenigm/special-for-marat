package ru.kpfu.itis.webchat.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.webchat.domain.Member;

import java.util.Base64;

@Component
public class JwtManager {
    private static long VALIDITY_TIME_MS = 60 * 60 * 1000;
    private static String key = Base64.getEncoder().encodeToString("secret-key".getBytes());

    public String createToken(Member member) {
        return Jwts.builder()
                .claim("id", member.getId())
                .claim("username", member.getUsername())
                .signWith(SignatureAlgorithm.HS256, key)
                .compact();
    }

    public boolean tokenIsValid(String token) {
        try {
            parseClaims(token);
        } catch (JwtException e) {
            return false;
        }
        return true;
    }

    public Member parseToken(String token) {
        Claims claims = parseClaims(token);
        return Member.builder()
                .id(claims.get("id", Long.class))
                .username(claims.get("username", String.class))
                .build();
    }

    private Claims parseClaims(String token) throws JwtException {
        return Jwts.parser()
                .setSigningKey(key)
                .parseClaimsJwt(token)
                .getBody();
    }
}
