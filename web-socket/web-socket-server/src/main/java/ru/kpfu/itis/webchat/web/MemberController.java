package ru.kpfu.itis.webchat.web;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.webchat.domain.Member;
import ru.kpfu.itis.webchat.service.MemberService;

@CrossOrigin
@RestController
public class MemberController {
    @Autowired
    private MemberService memberService;

    @Data
    private static class Request {
        private String username;
        private String password;
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/my")
    public Member myInfo() {
        return memberService.findMemberByToken(SecurityContextHolder
                .getContext().getAuthentication().getName()
        );
    }

    @PostMapping("/login")
    public String login(@RequestBody Request request) {
        return memberService.login(request.username, request.password);
    }

    @PostMapping("/register")
    public void register(@RequestBody Request request) {
        memberService.registration(request.username, request.password);
    }
}
