package ru.kpfu.itis.webchat.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.webchat.domain.Member;

@Component
public class JwtAuthenticationProvider implements AuthenticationProvider {
    @Autowired
    private JwtManager jwtManager;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String token = authentication.getName();
        if (jwtManager.tokenIsValid(token)) {
            Member extractedMember = jwtManager.parseToken(token);
            ((JwtAuthentication) authentication).setMember(extractedMember);
            authentication.setAuthenticated(true);
        } else {
            throw new BadCredentialsException("Invalid token");
        }
        return authentication;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return JwtAuthentication.class.equals(authentication);
    }
}
