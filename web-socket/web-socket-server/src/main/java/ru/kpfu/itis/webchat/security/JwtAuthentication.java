package ru.kpfu.itis.webchat.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import ru.kpfu.itis.webchat.domain.Member;

import java.util.Collection;

public class JwtAuthentication implements Authentication {
    private String token;
    private boolean authenticated;
    private Member member;

    public JwtAuthentication(String token) {
        this.token = token;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public Object getCredentials() {
        return member.getUsername();
    }

    @Override
    public Object getDetails() {
        return member;
    }

    @Override
    public Object getPrincipal() {
        return member;
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        this.authenticated = isAuthenticated;
    }

    @Override
    public String getName() {
        return token;
    }

    public void setMember(Member member) {
        this.member = member;
    }
}
