package ru.kpfu.itis.webchat.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import ru.kpfu.itis.webchat.domain.Member;
import ru.kpfu.itis.webchat.domain.Message;
import ru.kpfu.itis.webchat.service.MemberService;
import ru.kpfu.itis.webchat.service.MessageService;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class MessagesWebSocketHandler extends TextWebSocketHandler {
    private static Map<Long, WebSocketSession> sessions = new ConcurrentHashMap<>();

    @Autowired
    private MessageService messageService;

    @Autowired
    private MemberService memberService;

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
        String messageAsString = (String) message.getPayload();
        String token = SecurityContextHolder.getContext().getAuthentication().getName();
        Member member = memberService.findMemberByToken(token);

        messageService.sendMessage(Message.builder()
                .member(member)
                .text(messageAsString)
                .build());
        sessions.putIfAbsent(member.getId(), session);

        for (WebSocketSession currentSession : sessions.values()) {
            currentSession.sendMessage(new TextMessage(messageAsString));
        }
    }
}