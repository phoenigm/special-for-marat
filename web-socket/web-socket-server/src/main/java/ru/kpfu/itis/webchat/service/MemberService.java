package ru.kpfu.itis.webchat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import ru.kpfu.itis.webchat.domain.Member;
import ru.kpfu.itis.webchat.repository.MemberRepository;
import ru.kpfu.itis.webchat.security.JwtManager;

@CrossOrigin
@Service
public class MemberService {
    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private JwtManager jwtManager;

    public void registration(String username, String password) {
        memberRepository.save(Member.builder()
                .username(username)
                .password(passwordEncoder.encode(password))
                .build()
        );
    }

    public String login(String username, String password) {
        Member member = memberRepository.findByUsername(username)
                .orElseThrow(() -> new BadCredentialsException("Bad credentials"));

        if (!passwordEncoder.matches(password, member.getPassword())) {
            throw new BadCredentialsException("Bad credentials");
        }
        String token = jwtManager.createToken(member);
        memberRepository.save(member);
        return token;
    }

    public Member findMemberByToken(String token) {
        return memberRepository.findByToken(token)
                .orElseThrow(() -> new UsernameNotFoundException("Wrong token"));
    }
}
