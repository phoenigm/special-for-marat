package ru.kpfu.itis.webchat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.webchat.domain.Member;

import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {
    Optional<Member> findByUsername(String username);
    Optional<Member> findByToken(String token);
}