module.exports = {
    devServer: {
        proxy: {
            '/accounts/*': {
                target: "http://localhost:9000",
                changeOrigin: false,
            },

            '/oauth/*': {
                target: "http://localhost:9000",
                changeOrigin: false,
            }
        }
    }
};